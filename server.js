const express = require('express');

const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const mysql = require('mysql');
const cors = require('cors');
const app = express();

const sp = "@WSX-hJ5DKq=AT4^DhanPBS8Mwhd6bBR3mndxsxbhR^q+%MM@7cJRsgx5vFjKnrwxA&s=qrALjK";
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cors({
    origin: '*'
}));

app.get('/', (req,res)=> {
    res.send('Hello')
})

app.post('/api/login', (req,res) => {
    try{
        if(req.body.host === '' || typeof req.body.host === 'undefined') throw new Error("Host not set")
        if(req.body.port === '' || typeof req.body.port === 'undefined') throw new Error("Port not set")
        if(req.body.user === '' || typeof req.body.user === 'undefined') throw new Error("User not set")
        if(req.body.password === '' || typeof req.body.password === 'undefined') throw new Error("Password not set")
    
    
        const user = {
            "host": req.body.host,
            "port": req.body.port,
            "user": req.body.user,
            "password": req.body.password,
            "database": req.body.database,
        }
        
        
        jwt.sign(user, sp, { expiresIn: '30s' }, (err, token) => {
            if(err) throw err;
            res.json({"status":1,"token":token});
        });
    }catch (e){
        res.json({"status":-1,"msg":'Error'})
    }

})

app.post('/api/query', verifyToken, async (req,res) => {
    try{
        jwt.verify(req.token, sp, (err, decoded) => {
            if(err) throw res.json({"status":-99,"msg":'Invalid Token'});

            const dbSettings = {
                host     : decoded.host,
                port     : decoded.port,
                user     : decoded.user,
                password : decoded.password
            }
        
            const connection = mysql.createConnection(dbSettings);
        
            if(typeof req.body.query === 'undefined'){
                res.json({"status":-1,"msg":"Query not set"})
            }else{
                try {
                    connection.connect((err) => {
                        if(err){
                            
                            throw res.json({"status":-1,"msg":"Couldn't connect"});
                        } 
                    });
                    
                    connection.query(req.body.query, function (error, results, fields) {
                        if (error){
                            res.json({"status":-1,"msg":error.sqlMessage});
                        } else{
                            res.json(results);
                        }
                    });
                }catch (e){
                    res.json({"status":-1,"msg":e})
                } finally {
                    connection.end();
                }
        
            }
        });
    }catch(e){

    }

})

function verifyToken(req, res, next){
    const bearerHeader = req.headers['authorization'];
    if(typeof bearerHeader !== 'undefined'){
        const bearer = bearerHeader.split(' ');
        const bearerToken = bearer[1];
        req.token = bearerToken;
        next();
    }else{
        res.json({"status":-98,"msg":'Token no set'});
    }

}

app.listen('5000', () => console.log('Server started on port 5000'))